import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Reporter;
import static org.assertj.core.api.Assertions.assertThat;

public class steps {

    public void log(String msg) {
        Reporter.log(msg + "</br>");
        System.out.println(msg);
    }

    public JSONArray restaurantsListAPI(String location, String radius, String type, String key) {

        JSONObject resposeObjectForRestorantList = null;

        JSONArray restaurantsListAPIArray = new JSONArray();

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + location + "&radius=" + radius + "&type=" + type + "&key=" + key)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        try {
            Response response = client.newCall(request).execute();

            String a = response.body().string().toString();

            JSONParser responseParser = new JSONParser();

            resposeObjectForRestorantList = (JSONObject) responseParser.parse(a);

            int responceCode = response.code();
            assertThat(responceCode).isEqualTo(200);


            /*System.out.println(resposeObjectForRestorantList.toString());*/
            restaurantsListAPIArray.add(resposeObjectForRestorantList);

            try {
                String next_page_token = resposeObjectForRestorantList.get("next_page_token").toString();

                sleep(1);

            } catch (Exception e) {
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Array : " + restaurantsListAPIArray.toJSONString());

        return restaurantsListAPIArray;

    }

    public void sleep(int i) {
        try {
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}
