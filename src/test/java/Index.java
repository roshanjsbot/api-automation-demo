import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Index {

    @Test
    public void googleAPI() {
        String location = "45.371186, -75.716348";
        String radius = "50000";
        String type = "restaurant";
        String key= "AIzaSyCm21YnwKF8Xp1mVflCR2f_ysUjoaqWAcE";

        steps step = new steps();
        JSONArray restaurantsListAPI = step.restaurantsListAPI(location, radius,type, key);
        for(Object Obj: restaurantsListAPI){
            step.log(Obj.toString());
            JSONObject data = (JSONObject) Obj;
            step.log("Status : "+data.get("status").toString());
            assertThat(data.get("status").toString()).isEqualToIgnoringCase("ok");
        }
    }


}
